# -*- coding: utf-8 -*-
"""
Created on Sun May  8 01:11:49 2016

@author: Yorick
"""

"""Fichier d'installation de notre script salut.py."""
from cx_Freeze import setup, Executable
import sys
import os
from cx_Freeze import hooks
import scipy
#
def load_scipy_patched(finder, module):
#    """the scipy module loads items within itself in a way that causes
##        problems without the entire package and a number of other subpackages
##        being present."""
    finder.IncludePackage("scipy._lib")  # Changed include from scipy.lib to scipy._lib
    finder.IncludePackage("scipy.misc")
###
hooks.load_scipy = load_scipy_patched
currentFile = __file__
realPath = os.path.realpath(currentFile)
dirPath = os.path.dirname(realPath)

includefiles_list=[]

includefiles_list=[dirPath + '\\splash.png']
#scipy_path ='C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\io'
#includefiles_list.append('C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\special\\_ufuncs.pyd')
#includefiles_list.append('C:\\python_27\\python-2.7.10.amd64\\Lib\\site-packages\\scipy\\special\\_ufuncs_cxx.pyd')

# exclude unneeded packages. More could be added. Has to be changed for
# other programs.
excludes=['Tkinter', 'zmq','matplotlib', 'scipy.spatial.cKDTree', 'debug']
optimize=2
#excludes=['collections.abc']

os.environ['TCL_LIBRARY']='D:\\PROGRAMMES\\WinPython-64bit-3.6.3.0Qt5\\python-3.6.3.amd64\\tcl\\tcl8.6'
os.environ['TK_LIBRARY']='D:\\PROGRAMMES\\WinPython-64bit-3.6.3.0Qt5\\python-3.6.3.amd64\\tcl\\tk8.6'
#bin_includes = ["pywintypes34.dll"]

bin_includes = []
packages=[]
includes = ["sip","re","atexit", 'atexit', 'numpy.core._methods', 'numpy.lib.format','numpy.linalg', 'timeit', 'scipy', 'scipy.ndimage._ni_support', 'pyaudio', 'pyqtgraph', 'cProfile', 'pyqtgraph.ThreadsafeTimer', 'pyqt5', 'pyqtgraph.debug']
#includes = ["sip","re","atexit", 'atexit', 'numpy.core._methods', 'numpy.lib.format', 'timeit', 'scipy', 'numpy', 'scipy.ndimage._ni_support', 'pyaudio', 'pyqtgraph', 'cProfile', 'pyqtgraph.ThreadsafeTimer', 'pyqt5', 'pyqtgraph.debug']
#includes = ["sip","re","atexit", 'atexit', 'numpy.core._methods', 'numpy.lib.format', 'timeit', 'numpy', 'scipy.ndimage._ni_support', 'pyaudio', 'pyqtgraph', 'cProfile', 'pyqtgraph.ThreadsafeTimer', 'pyqt5', 'pyqtgraph.debug']
build_exe_options = {"packages":packages,"optimize":optimize,"include_files":includefiles_list,"includes":includes,"excludes":excludes, "bin_includes": bin_includes}#,"zip_include_packages":"*","zip_exclude_packages":includes}
# Information about the program and build command. Has to be adjusted for
# other programs
setup(
    name="SOFT_PARTIEL",                           # Name of the program
    version="1.0",                              # Version number
    description="SOUND GENERATOR",     # Description
    author="Yorick Buot de l'Epine",
    author_email="ybuotdel@utc.fr",
    options = {"build_exe": build_exe_options}, # <-- the missing line
    executables=[Executable(dirPath + "\\synthtone_main.py",
                            icon= dirPath + '\\logo_synthtone.ico',# Executable python file
#                            compress=True,
                            )],)
