#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2017 Yorick Buot de l'Epine

#
# Cuttone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.
#
# Cuttone is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
import scipy
from scipy import signal
from scipy import fftpack
import os.path
import os
os.environ['ETS_TOOLKIT'] = 'qt5'
import sip
sip.setapi('QString', 2)
os.environ['QT_API'] = 'pyqt'
####################################
from PyQt5 import QtGui,QtCore,QtWidgets
#from PyQt5.QtCore import SIGNAL
import pyqtgraph as pg
import pyaudio
import main_gui
import param_gui
import filter_gui
#import splashscreen
#import scipy.io.wavfile
import wave
#import sys
import psutil, os

def kill_proc_tree(pid, including_parent=True):
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()


class plot_sig:
    def __init__(self,p,var):
        pen=pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground('w')
        #self.p.getPlotItem().hideButtons()
        #self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        self.courbe=self.p.plot()
        self.p.disableAutoRange()
        self.p.setClipToView(True)
        self.p.setDownsampling(ds=5, auto=True, mode='peak')
        self.courbe.setData(scipy.linspace(0,var.time_max,50),scipy.zeros((50)),pen=pg.mkPen('b',width=2))
        self.env=self.p.plot()
        self.env.setData(scipy.linspace(0,var.time_max,50),scipy.zeros((50)),pen=pg.mkPen('r',width=2))
        self.p.setXRange(0,50)
        self.p.setYRange(-1.05,1.05)
        #mise en forme axe#######
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (V)')
        self.p.setLabel('bottom',text ='Temps (s)')
    def update_data(self,X,Y):
         self.courbe.setData(X,Y,pen=pg.mkPen(color='b',width=0.9))
         self.p.setXRange(0,X[-1])
         self.p.setYRange(-1.05,1.05)
    def update_env(self,X,Y):
         self.env.setData(X,Y,pen=pg.mkPen(color='r',width=0.9))
         self.p.setXRange(0,X[-1])
         self.p.setYRange(-1.05,1.05)
    def suppr(self):
        self.p.clear()

class plot_spectrum:
    def __init__(self,p,var):
        pen=pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground('w')
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        colortable=color_table()
        #colortable=cubehelix()
        #colortable=rainbow()
        self.image=pg.ImageItem()
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        Xaxis.enableAutoSIPrefix(False)
        Xaxis.enableAutoSIPrefix(False)
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*scipy.size(var.data_freq)/ntick))
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*scipy.size(var.spectro_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        self.p.setLabel('left', text ='<font size="4">Frequency </font>', units ='<font size="4">Hz </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Seconds </font>')


    def reset(self,var):
        self.p.removeItem(self.image)
        self.p.removeItem(self.p.getPlotItem().getAxis('bottom'))
        self.p.removeItem(self.p.getPlotItem().getAxis('left'))
        colortable=color_table()
        #colortable=cubehelix()
        #colortable=rainbow()
        self.image=pg.ImageItem()
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
    ############## define Ticks ###############
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        ntick=5.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.1f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(round(ii*scipy.size(var.data_freq)/ntick))
            Xtick_string.append("{0:.1f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(round(ii*scipy.size(var.spectro_time)/ntick))
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        ##########################- define cross ###################
        self.image.setZValue(-1) #◙remet l'image à l'arrière plan

    def update_data(self,mat,db_min,db_max):
         self.image.setImage(mat)
         self.image.setLevels([db_min,db_max])


def downsample(X,Y,max_point):
    if len(X)>max_point:
        delta=max(1,int(len(X)/float(max_point)))
        X=X[0::delta]
        Y1=scipy.zeros(len(X))
        for ii in range(0,len(X)):
            dat=Y[delta*ii:delta*ii+delta]
            #Y1[ii]=dat[scipy.where(abs(dat)==scipy.amax(abs(dat)))[0][0]]
            Y1[ii]=dat[scipy.random.randint(len(dat),size=1)]
        #Y1[ii]=scipy.mean(Y[delta*ii:delta*ii+delta])
    else:
        Y1=Y
    return X,Y1

class PARTIEL:
    def _init_(self):
        self.freq=[]
        self.amp=[]
        self.phase=[]
        self.tau=[]

class FILTER:
    def _init_(self):
        self.type=[]
        self.freq1=[]
        self.freq2=[]
        self.order=[]

class Variable:
    def __init__(self):
        self.freq_max = [] #max frequency to study
        self.buffer_size = [] # size of buffer
        self.delta_freq=[] #
        self.window=[] # windowed type
        self.covering=[] #covering for windowed
        self.channel=[] # voie
        self.time_max=[]
        self.data_time=[]
        self.spectro_time=[]
        self.data_freq=[]
        self.nb_cover=[]
        self.db_min=[]
        self.db_max=[]
        self.factor_rate=2.56
        self.enveloppe_X=[]
        self.enveloppe_Y=[]

    ############Î definie la table des couleurs
def cubehelix(gamma=1, s=0.5, r=-1.5, h=1.0):
    def get_color_function(p0, p1):
        def color(x):
            xg = x ** gamma
            a = h * xg * (1 - xg) / 2
            phi = 2 * scipy.pi * (s / 3 + r * x)
            return xg + a * (p0 * scipy.cos(phi) + p1 * scipy.sin(phi))
        return color
    array = scipy.empty((256, 3))
    abytes = scipy.arange(0, 1, 1/256.)
    array[:, 0] = get_color_function(-0.14861, 1.78277)(abytes) * 255
    array[:, 1] = get_color_function(-0.29227, -0.90649)(abytes) * 255
    array[:, 2] = get_color_function(1.97294, 0.0)(abytes) * 255
    return array
#
def rainbow():
    lut = scipy.empty((256, 3))
    abytes = scipy.arange(0, 1, 0.00390625)
    lut[:, 0] = scipy.absolute(2 * abytes - 0.5) * 255
    lut[:, 1] = scipy.sin(abytes * scipy.pi) * 255
    lut[:, 2] = scipy.cos(abytes * scipy.pi / 2) * 255
    return lut

def color_table():
    pos = scipy.array([0.0, 0.5, 1.0])
    #pos = scipy.array([0.0, 1.0])
    color = scipy.array([[26,82,118,255], [20,143,119,255], [241,196,15,255]], dtype=scipy.ubyte)
#    color = scipy.array([[0,0,0,255],[0,255,255,255]], dtype=scipy.ubyte)
    map = pg.ColorMap(pos, color)
    lut = map.getLookupTable(0.0, 1.0, 256)
    return lut

def colormap_gui(fig,min_val,max_val):
    nb_color=255
    value=scipy.array([scipy.linspace(min_val,max_val,nb_color),scipy.linspace(min_val,max_val,nb_color)])
    #▲colortable=cubehelix()
#    colortable=rainbow()
    colortable=color_table()
    fig.clear()
    pen=pg.mkPen(color='k', width=2)
    image=pg.ImageItem()
    item_color=fig.getPlotItem()
    image.setLookupTable(colortable)
    fig.setBackground('w')
    fig.getPlotItem().hideButtons()
    fig.getPlotItem().setMouseEnabled(x=False,y=False)
    fig.addItem(image)
    image.setImage(value,autoDownSample=True)
    image.setLevels([min_val,max_val])
    Xaxis=item_color.getAxis('bottom')
    Yaxis=item_color.getAxis('left')
    pen=pg.mkPen(color='k', width=2)
    fig.plotItem.getAxis('left').setPen(pen)
    fig.plotItem.getAxis('bottom').setPen(pen)
    ntick=7
    Ytick_loc=[]
    Ytick_string=[]
    for ii in range(0,ntick+1):
        Ytick_string.append("{0:.1f}".format(round(min_val+(ii*((max_val-min_val)/ntick)))))
        Ytick_loc.append(round((ii*((nb_color)/ntick))))
    Ytick=zip(Ytick_loc,Ytick_string)
    Yaxis.setTicks([list(Ytick)])
    #Yaxis.setPen(pen)
    #Xaxis.setTicks([1,'dB'])
    #Xaxis.setPen(pen)
class splashScreen:
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)

    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()

class filter_window(QtGui.QWidget,filter_gui.Ui_filter):
    def __init__(self, parent=None):
        super(filter_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Filter View")

class param_window(QtGui.QWidget,param_gui.Ui_param):
    def __init__(self, parent=None):
        super(param_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Parameter")
    ############### création des signaux #######################

class spectro(QtGui.QMainWindow,main_gui.Ui_MainWindow):
    def __init__(self, parent=None):
        super(spectro, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"PARTIEL")
        ############### definie la fenetre des paramètre ########
        self.param = param_window()
        ############# initalise variable ###################
        os.chdir('C:\\')
        self.variable=Variable()
        self.acqui=0
        ###############################################
        self.noise_edit.insert('0')
        self.time_edit.insert('2')
        self.sampling_edit.insert('8096')
        self.db_min_edit.insert('-140')
        self.db_max_edit.insert('0')
        #################################################
        self.AM_freq_edit.insert('1000')
        self.AM_index_edit.insert('0.5')
        #########################################################
        self.FM_freq_edit.insert('1000')
        self.FM_amp_edit.insert('1')
        self.FM_sensi_edit.insert('0.5')
        #########################################################
        self.variable.freq_max=float(self.sampling_edit.text())/float(self.variable.factor_rate)
        self.param.freq_edit.setText("{0:.1f}".format(self.variable.freq_max))
        self.param.line_box.addItems(['100','200','400','800','1600','3200'])
        self.param.line_box.setCurrentIndex(2)
        self.variable.buffer_size =int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.param.fenetre_box.addItems(['Uniform','Hanning'])
        self.param.fenetre_box.setCurrentIndex(1)
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.param.recouvrement_edit.insert('0.66')
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_delta_f() # définie le delta f
        ############# inti varibale for initiale plot #############################
        self.define_colormap()
        self.define_data_freq_time()
        self.signal_plot=plot_sig(self.plot_signal,self.variable)
        ################### defini l'enveloppe
        pos=scipy.linspace(0,self.variable.time_max,5)
        val_h=[1,1,1,0.5,0]
        handle=zip(pos,val_h)
        self.enveloppe_roi=pg.PolyLineROI(handle,closed=False,movable=False,pen=pg.mkPen('g',width=0.9))
        self.enveloppe_roi.handlePen=pg.mkPen('b')
        hand=self.enveloppe_roi.getHandles()
        for ii in range(0,scipy.size(hand)):
            hand[ii].pen=pg.mkPen('b')
            hand[ii].currentPen=pg.mkPen('b')
        self.signal_plot.p.addItem(self.enveloppe_roi)
        self.modify_enveloppe()
        #############################################################
        self.spectro_plot=plot_spectrum(self.plot_spectrum,self.variable)
        ##########defini les tableaux ##########################
        self.partiel_table.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.partiel_list=[]
        self.filter_table.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.filter_liste=[]
        self.liste_filter_button=[]
        ##########################################################
        #self.clean_partiel_button.setEnabled(False)
        #self.clean_filter_button.setEnabled(False)
        self.listen_button.setEnabled(False)
        self.save_button.setEnabled(False)
        self.connectActions()

    def connectActions(self):
        self.param_button.clicked.connect(self.param_open)
        self.param.enregistrement_button.clicked.connect(self.save_param)
        self.param.freq_edit.editingFinished.connect(self.define_delta_f)
        self.param.line_box.currentIndexChanged.connect(self.define_delta_f)
        self.param.fenetre_box.currentIndexChanged.connect(self.define_window)
        ############################################################################
        self.add_freq_button.clicked.connect(self.add_freq)
        self.add_filter_button.clicked.connect(self.add_filter)
        self.listen_button.clicked.connect(self.listen_sig)
        self.db_max_edit.editingFinished.connect(self.define_colormap)
        self.db_min_edit.editingFinished.connect(self.define_colormap)
        self.sampling_edit.editingFinished.connect(self.actualise_param)
        self.time_edit.editingFinished.connect(self.actualise_param)
        self.clean_partiel_button.clicked.connect(self.clean_partiel)
        self.clean_filter_button.clicked.connect(self.clean_filter)
        self.enveloppe_box.stateChanged.connect(self.modify_enveloppe)
        #self.enveloppe_roi.sigRegionChanged.connect(self.modify_enveloppe)
        self.save_button.clicked.connect(self.save_wave)
        self.create_button.clicked.connect(self.main)
        #self.add_button.clicked.connect(self.add_roi)
        #self.undo_button.clicked.connect(self.undo)
        #self.generate_button.clicked.connect(self.generate)

    def param_open(self):
        """Lance la deuxième fenêtre"""
        self.param.show()

    def save_param(self):
        self.variable.freq_max = int(float(self.param.freq_edit.text()))#Emax frequency to study
        self.variable.buffer_size =int(float(self.variable.factor_rate*int(self.param.line_box.currentText()))) # size of buffer
        self.variable.window=self.param.fenetre_box.currentText()  # windowed type
        self.variable.covering=float(self.param.recouvrement_edit.text()) #covering for windowed
        self.define_data_freq_time()
        ########♣ remet les slide au milieu des plage de frequence et de temps
        self.param.close()

    def define_data_freq_time(self):
        delta_f=self.variable.delta_freq# equivalant au nombre de buffer à remplir par seconde
        self.variable.nb_cover=int(round(1/(1-self.variable.covering)))
        self.variable.data_freq=scipy.arange(0,self.variable.freq_max,delta_f)#[0:-1]
        self.variable.time_max=float(self.time_edit.text())
        self.variable.data_time=scipy.arange(0,self.variable.time_max,1/float(self.variable.factor_rate*self.variable.freq_max))

        self.variable.spectro_time=scipy.linspace(0,self.variable.time_max,
                                               round(self.variable.nb_cover*self.variable.time_max*self.variable.delta_freq))
        ################## créer les buffers ##############################################
        ###### Signal audio pour toute la duree de l'enregistrement #####
#        ##################################################################################
        self.signal_buffer=1e-12*scipy.ones(scipy.size(self.variable.data_time))
        ###################### spectre sur toute la durrée de l'enregistrement + nouveau spectre generé ###########
        self.spectro_buff=1e-7*scipy.ones((scipy.size(self.variable.data_freq),scipy.size(self.variable.data_time)))

    def define_window(self):
        fenetre=self.param.fenetre_box.currentText()
        if fenetre=='Uniform':
            self.param.recouvrement_edit.setText('0')
        if fenetre=='Hanning':
            self.param.recouvrement_edit.setText('0.66')

    def define_delta_f(self):
        self.param.enregistrement_button.setEnabled(True)
        freq_max=self.variable.freq_max
        ####################################################################
#        if freq_max>=500 and freq_max<=9000:
        buffer =self.variable.factor_rate*int(self.param.line_box.currentText()) # size of buffer
        self.param.enregistrement_button.setEnabled(True)
        self.variable.delta_freq=int(self.variable.factor_rate*freq_max)/float(buffer)
        self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))

    def define_colormap(self):
        val_min=int(self.db_min_edit.text())
        val_max=int(self.db_max_edit.text())
        colormap_gui(self.colorbar_plot,val_min,val_max)
        self.variable.db_min=val_min
        self.variable.db_max=val_max
        ############# si pas d'acquisition, on replote le spectre du buffer #####
        if self.acqui==1:
            self.spectro_plot.update_data(self.spectro_buff.T,self.variable.db_min,self.variable.db_max)

    def add_freq(self):
        max_row=self.partiel_table.rowCount()
        item=QtGui.QTableWidgetItem()
        item.setText('Partiel '+str(max_row))
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        item.setCheckState(QtCore.Qt.Checked)
        ################################################
        self.partiel_table.insertRow(max_row)
        self.partiel_table.setItem(max_row,0,item)
        self.partiel_table.setItem(max_row,1,QtGui.QTableWidgetItem(str(100)))
        self.partiel_table.setItem(max_row,2,QtGui.QTableWidgetItem(str(50)))
        self.partiel_table.setItem(max_row,3,QtGui.QTableWidgetItem(str(0)))
        self.partiel_table.setItem(max_row,4,QtGui.QTableWidgetItem(str(0.005)))
        ###############################################♣

    def add_filter(self):
        max_row=self.filter_table.rowCount()
        item=QtGui.QTableWidgetItem()
        item.setText('Filter '+str(max_row))
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        item.setCheckState(QtCore.Qt.Checked)
        ################################################
        combo=QtGui.QComboBox()
        combo.addItems(['LowPass','HighPass','BandPass','BandStop'])
        combo.setCurrentIndex(1)
        ###################################################
        button=QtGui.QPushButton()
        button.setText('View')
        self.liste_filter_button.append(button)
        button.clicked.connect(lambda:self.view_filter(max_row))
        ################################################
        self.filter_table.insertRow(max_row)
        self.filter_table.setItem(max_row,0,item)
        self.filter_table.setCellWidget(max_row,1,combo)
        self.filter_table.setItem(max_row,2,QtGui.QTableWidgetItem(str(100)))
        self.filter_table.setItem(max_row,3,QtGui.QTableWidgetItem(str(1000)))
        self.filter_table.setItem(max_row,4,QtGui.QTableWidgetItem(str(2)))
        self.filter_table.setCellWidget(max_row,5,button)
        ###############################################

    def modify_enveloppe(self):
        if self.enveloppe_box.isChecked()==True:
            self.signal_plot.p.addItem(self.enveloppe_roi)
            self.signal_plot.p.addItem(self.signal_plot.env)
            hand=self.enveloppe_roi.getHandles()# a améliorer
            self.variable.enveloppe_X=self.variable.data_time
            tin=[]
            valin=[]
            for ii in range(0,scipy.size(hand)):
                h=self.enveloppe_roi.getLocalHandlePositions(ii)
                pos=h[1]
                tin.append(float(pos.x()))
                valin.append(float(pos.y()))
                if valin[ii]<0:
                    valin[ii]=0
                if valin[ii]>1:
                    valin[ii]=1
            try:
                self.variable.enveloppe_Y=scipy.interp(self.variable.enveloppe_X,tin,valin)
                self.signal_plot.update_env(self.variable.enveloppe_X,self.variable.enveloppe_Y)
            except:
                pass
        else:
            self.signal_plot.p.removeItem(self.enveloppe_roi)
            self.signal_plot.p.removeItem(self.signal_plot.env)


    def actualise_param(self):
        self.acqui=0
        self.variable.time_max=float(self.time_edit.text())
        self.variable.freq_max=float(self.sampling_edit.text())/(self.variable.factor_rate)
        self.param.freq_edit.setText("{0:.3f}".format(self.variable.freq_max))
        self.define_delta_f()
        self.define_data_freq_time()
        self.modify_enveloppe()
        self.spectro_plot.reset(self.variable)
        self.define_colormap()
        self.listen_button.setEnabled(False)
        self.save_button.setEnabled(False)

    def view_filter(self,row):
        row=row#
        self.filt_window=filter_window()
        self.filt_window.label_filter.setText('Filter')
        #######################################################
        filter=FILTER()
        filter.type=self.filter_table.cellWidget(row,1).currentIndex()
        if float(self.filter_table.item(row,2).text())>(self.variable.freq_max):
            self.filter_table.setItem(row,2,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
            filter.freq1=self.variable.freq_max
        else:
            filter.freq1=float(self.filter_table.item(row,2).text())
        ##################################
        if float(self.filter_table.item(row,3).text())>(self.variable.freq_max):
            self.filter_table.setItem(row,3,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
            filter.freq2=self.variable.freq_max
        else:
            filter.freq2=float(self.filter_table.item(row,3).text())
        #########################
        if float(self.filter_table.item(row,4).text())>8:
            self.filter_table.setItem(row,4,QtGui.QTableWidgetItem(str(8)))
            filter.order=8
        else:
            filter.order=float(self.filter_table.item(row,4).text())
        #self.filter_list.append(filter)
        ###########################################################
        if filter.type==0:
                [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'lowpass')# frequence coupure  basée sur la fréquence de Nquyst soit fmax/2
        if filter.type==1:
                [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'highpass')
        if filter.type==2:
                flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandpass')
        if filter.type==3:
                flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandstop')
        [w, h] = scipy.signal.freqz(C, D)
        ################################################
        h[scipy.where(abs(h)==0)]=1e-2
        w[scipy.where(abs(w)==0)]=1e-2
        ##### display #####
        pen=pg.mkPen(color='k', width=2)
        p = self.filt_window.plot_filter
        p.setBackground('w')
        courbe=p.plot()
        courbe.setData(scipy.linspace(1,1000,50),scipy.zeros((50)),pen=pg.mkPen('b',width=2))
        p.setClipToView(True)
        p.plotItem.getAxis('left').setPen(pen)
        p.plotItem.getAxis('bottom').setPen(pen)
        p.plotItem.showGrid(x=True,y=True)
        p.setLogMode(x=True,y=False)
        p.setLabel('left', text ='Amplitude (dB)')
        p.setLabel('bottom',text ='Frequency (Hz)')
#        courbe.setData(w*(self.variable.freq_max*self.variable.factor_rate)/float(2*scipy.pi),abs(h),pen=pg.mkPen(color='b',width=2))
        courbe.setData(w[1:-1]*(self.variable.freq_max*self.variable.factor_rate)/float(2*scipy.pi), 20*scipy.log10(abs(h[1:-1])),pen=pg.mkPen(color='b',width=2))
        self.filt_window.show()



    def clean_partiel(self):
        while self.partiel_table.rowCount() > 0:
            self.partiel_table.removeRow(0)

    def clean_filter(self):
        while self.filter_table.rowCount() > 0:
            self.filter_table.removeRow(0)

    def main(self):
        ################################
        # INITIALISE
        #################################
        self.listen_button.setEnabled(True)
        self.save_button.setEnabled(True)
        self.define_data_freq_time()
        self.modify_enveloppe()
        self.acqui=1
        ####################################
        # lecture des partiels et creation du signal
        ####################################
        row=self.partiel_table.rowCount()
        #`self.partiel_list=[]
        for ii in range(0,row):
            if self.partiel_table.item(ii,0).checkState()==QtCore.Qt.Checked:
                partiel=PARTIEL()
                partiel.freq=float(self.partiel_table.item(ii,1).text())
                ################################""
                partiel.amp=float(self.partiel_table.item(ii,2).text())/float(100)
                if abs(float(self.partiel_table.item(ii,3).text()))>180:
                    partiel.phase=scipy.sign(float(self.partiel_table.item(ii,3).text()))*scipy.py
                    self.partiel_table.setItem(ii,3,QtGui.QTableWidgetItem(str(partiel.phase)))
                else:
                    partiel.phase=float(self.partiel_table.item(ii,3).text())*scipy.pi/float(180)
                ############################################
                if float(self.partiel_table.item(ii,4).text())>1:
                    partiel.tau=1
                    self.partiel_table.setItem(ii,4,QtGui.QTableWidgetItem(str(1)))
                elif float(self.partiel_table.item(ii,4).text())<0:
                    partiel.tau=0
                    self.partiel_table.setItem(ii,4,QtGui.QTableWidgetItem(str(0)))
                else:
                    partiel.tau=float(self.partiel_table.item(ii,4).text())
                #self.partiel_list.append(partiel)
                sig_partiel=partiel.amp*scipy.cos(partiel.freq*2*scipy.pi*self.variable.data_time-partiel.phase)
                lamb=2*scipy.pi*partiel.freq*partiel.tau/float(len(self.variable.data_time))#
                sig_partiel=sig_partiel*scipy.exp(-scipy.arange(scipy.size(self.variable.data_time))*lamb)
                self.signal_buffer=self.signal_buffer+sig_partiel
    ################################################
    # Apllication de la modulation de frequence
    ################################################
        if self.FM_box.isChecked()==True:
            FM_sensi=float(self.FM_sensi_edit.text())
            FM_freq=float(self.FM_freq_edit.text())
            FM_amp=float(self.FM_amp_edit.text())
            FM_signal=scipy.zeros(len(self.signal_buffer))
            for ii in range(0,len(FM_signal)):
                integ=scipy.trapz(self.signal_buffer[0:ii])
                FM_signal[ii]=FM_amp*scipy.cos(2*scipy.pi*FM_freq*self.variable.data_time[ii]+2*scipy.pi*FM_sensi*integ)#page 312 analog and digital  signal processing
            self.signal_buffer=FM_signal

        ################################################
        # Apllication de la modulation d'amplitude
        ################################################
        if self.AM_box.isChecked()==True:
            AM_index=float(self.AM_index_edit.text())
            AM_freq=float(self.AM_freq_edit.text())
            amp_max=scipy.amax(abs(self.signal_buffer))
            AM_amp=amp_max/float(AM_index)
            AM_signal=scipy.zeros(len(self.signal_buffer))
            AM_signal=AM_amp*scipy.cos(2*scipy.pi*self.variable.data_time*AM_freq)*(1+(AM_index/float(amp_max))*self.signal_buffer)#page 309 analog and digital  signal processing
            self.signal_buffer=AM_signal
        ####################################
        # Application de l'enveloppe
        #####################################
        if self.enveloppe_box.isChecked()==True:
            self.signal_buffer=self.signal_buffer*self.variable.enveloppe_Y
        #######################################
        # ajout du bruit
        #####################################
        if self.noise_box.isChecked()==True:
            noise_level=float(self.noise_edit.text())
            if noise_level<0:
                noise_level=0
            noise=scipy.random.rand(scipy.size(self.variable.data_time))*noise_level
            self.signal_buffer=self.signal_buffer+noise
        #########################################
        # application des filtres
        ##########################################
        row=self.filter_table.rowCount()
        #self.filter_list=[]
        for ii in range(0,row):
            if self.filter_table.item(ii,0).checkState()==QtCore.Qt.Checked:
                filter=FILTER()
                filter.type=self.filter_table.cellWidget(ii,1).currentIndex()
                if float(self.filter_table.item(ii,2).text())>(self.variable.freq_max):
                    self.filter_table.setItem(ii,2,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
                    filter.freq1=self.variable.freq_max
                else:
                    filter.freq1=float(self.filter_table.item(ii,2).text())
                    ##################################
                if float(self.filter_table.item(ii,3).text())>(self.variable.freq_max):
                    self.filter_table.setItem(ii,3,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
                    filter.freq2=self.variable.freq_max
                else:
                    filter.freq2=float(self.filter_table.item(ii,3).text())
                    #########################
                if float(self.filter_table.item(ii,4).text())>10:
                    self.filter_table.setItem(ii,4,QtGui.QTableWidgetItem(str(10)))
                    filter.order=10
                else:
                    filter.order=float(self.filter_table.item(ii,4).text())
                #self.filter_list.append(filter)
                ###########################################################
                if filter.type==0:
                    [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'lowpass')# frequence coupure  basée sur la fréquence de Nquyst soit fmax/2
                if filter.type==1:
                    [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'highpass')
                if filter.type==2:
                    flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                    fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                    [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandpass')
                if filter.type==3:
                    flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                    fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                    [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandstop')
                self.signal_buffer=scipy.signal.filtfilt(C,D,self.signal_buffer)
        ####################################################################
        self.signal_buffer[scipy.where(self.signal_buffer==0)]=1e-12
        if scipy.amax(abs(self.signal_buffer))>1:
                self.signal_buffer=self.signal_buffer/scipy.amax(abs(self.signal_buffer))# normalisation des signaux avant d'appliquer l'enveloppe
        ##########################################
        # visualisation  signal
        ############################################
        #[X,Y]=downsample(self.variable.data_time,self.signal_buffer,2000)
        self.signal_plot.update_data(self.variable.data_time,self.signal_buffer)
        #self.signal_plot.update_data(X,Y)
        ################################################
        # visualisation spectro
        ###############################################
        nb_freq=self.variable.buffer_size
        overlap=self.variable.covering
        if self.variable.window=='Uniform':
            window='boxcar'
        elif self.variable.window=='Hanning':
            window='hann'
        freq_sample=self.variable.freq_max*self.variable.factor_rate
        f, t, Sxx = signal.spectrogram(self.signal_buffer,freq_sample,window='hann',nperseg=nb_freq,noverlap=int((overlap)*nb_freq),nfft=nb_freq, detrend='constant', return_onesided=True, scaling='spectrum', axis=-1, mode='magnitude')
        self.spectro_buff=20*scipy.log10(abs(2*Sxx))
        self.spectro_plot.update_data(self.spectro_buff.T,self.variable.db_min,self.variable.db_max)
        app.processEvents()


    def listen_sig(self):
        self.listen_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        BUFFER=self.variable.buffer_size
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels= 2,rate=RATE,input=False,output=True,frames_per_buffer=BUFFER)
        data=scipy.zeros((len(self.signal_buffer),2))
        data[:,0]=self.signal_buffer
        data[:,1]=self.signal_buffer
        stream.write(data.astype(scipy.float32).tostring())
        stream.close()
        p.terminate()
#        QtCore.QThread.sleep(self.variable.time_max)
        self.listen_button.setEnabled(True)
        app.processEvents()

    def save_wave(self):
        dataW=scipy.zeros((scipy.size(self.signal_buffer),2))
        dataW[:,0]=scipy.multiply(self.signal_buffer,32767).astype(scipy.int16)
        dataW[:,1]=scipy.multiply(self.signal_buffer,32767).astype(scipy.int16)
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','partiel.wav',filter ='wav (*.wav *.)')
        print(filename)
        wave_file = wave.open(filename[0], 'w')
        wave_file.setparams((2, 2, RATE, 0, 'NONE', 'not compressed'))
        for ii in range(0,scipy.size(dataW[:,0])):
            sig= wave.struct.pack('<hh', int(dataW[ii,0]),int(dataW[ii,1]))
            wave_file.writeframesraw(sig)
        wave_file.close()



if __name__=='__main__':
    import sys
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    gui = spectro()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-
    me = os.getpid()
    kill_proc_tree(me)
