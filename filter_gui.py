# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'filter_plot.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_filter(object):
    def setupUi(self, filter):
        filter.setObjectName("filter")
        filter.resize(699, 546)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(filter.sizePolicy().hasHeightForWidth())
        filter.setSizePolicy(sizePolicy)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(filter)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetMinimumSize)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_filter = QtWidgets.QLabel(filter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_filter.sizePolicy().hasHeightForWidth())
        self.label_filter.setSizePolicy(sizePolicy)
        self.label_filter.setObjectName("label_filter")
        self.verticalLayout.addWidget(self.label_filter)
        self.plot_filter = PlotWidget(filter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.plot_filter.sizePolicy().hasHeightForWidth())
        self.plot_filter.setSizePolicy(sizePolicy)
        self.plot_filter.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.CrossCursor))
        self.plot_filter.setObjectName("plot_filter")
        self.verticalLayout.addWidget(self.plot_filter)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(filter)
        QtCore.QMetaObject.connectSlotsByName(filter)

    def retranslateUi(self, filter):
        _translate = QtCore.QCoreApplication.translate
        filter.setWindowTitle(_translate("filter", "Form"))
        self.label_filter.setText(_translate("filter", "label_filter"))
from pyqtgraph import PlotWidget
