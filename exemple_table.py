# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 11:39:18 2019

@author: YORICK
"""

import scipy as sc
from DWDataReaderHeader import *
from ctypes import *
import _ctypes
import os.path
import os
os.environ['ETS_TOOLKIT'] = 'qt5'
import sip
sip.setapi('QString', 2)
os.environ['QT_API'] = 'pyqt'
####################################
import psutil
from PyQt5 import QtGui,QtCore
import pyqtgraph as pg
import stat_douai_GUI3
import calcul_bar_GUI
from stat_douai_graphique import calcul_save,graphique,plot1D
from DXD_file_def import CONFIG_DXD,get_DXD_config,get_data


import struct
bit=struct.calcsize("P") * 8
if bit==64:
    lib = cdll.LoadLibrary('DWDataReaderLib64.dll')
else:
    lib = cdll.LoadLibrary('DWDataReaderLib.dll')
    
class DATA():# classe contenant les données pour les 3 types de voies (temporelle,frequentielle et indicateurs)
    def __init__(self):
        self.indice_ind=[]
        self.indice_freq=[]
        self.indice_temp=[]
        self.data_temp=[]
        self.data_freq=[]
        self.data_indicator=[]
        self.stat_temp=[]
        self.stat_freq=[]
        self.stat_indicator=[]
        self.recap_temp=[]
        self.recap_freq=[]
        self.recap_indicator=[]
    def reset(self):
        self.indice_ind=[]
        self.indice_freq=[]
        self.indice_temp=[]
        self.data_temp=[]
        self.data_freq=[]
        self.data_indicator=[]
        self.recap_temp=[]
        self.recap_freq=[]
        self.recap_indicator=[]

class Variable():# calsse conteant les variables globales utilisées dans tout le soft
     def __init__(self):
        self.downsample=[]# valeur du downsampling temporel
        self.nom_fichier = [] #nom des fichier de mesures
        self.nom_variable= [] # nom des variables dans les fichiers
        self.point_temp=[] # nombre de point temporelle
        self.point_freq=[] # nombre de point freq
        self.graph_object=[]# list de graphe
        self.list_data_index=[]
        self.max_point=5000
        self.nb_bar=20# nombre de barres dans les histogram
     def reset(self):
        self.downsample=[]# valeur du downsampling temporel
        self.nom_fichier = [] #nom des fichier de mesures
        self.nom_variable= [] # nom des variables dans les fichiers
        self.point_temp=[] # nombre de point temporelle
        self.point_freq=[] # nombre de point freq
        self.graph_object=[]# list de graphe
        self.list_data_index=[]
        self.max_point=50000# nombre d epoint à afficher
        self.nb_bar=20# nombre de barres dans les histogram
    
def kill_proc_tree(pid, including_parent=True):    
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()

class calcul_bar(QtGui.QWidget,calcul_bar_GUI.Ui_launch):# fonction gérant l'affichage de la fenetre calcul-->bar de progression
    def __init__(self, parent=None):
        super(calcul_bar, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Progression")

      
class splashScreen:# classe gérant le splashscreen
    def __init__(self):
        self.splash=QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\\splashscreen.png')
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint |
                            QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix))                     # 10 = Windowrole
        self.splash.setPalette(palette)
        
    def show_splash(self):
        self.splash.show()
    def close_splash(self):
        self.splash.close()
           
def statistique(data,unit,type_ch):# fonction qui calcule les statisitques (moyennes, ecart-type, min/max)
    try:
        n=sc.size(data[:,0])
    except:
        n=sc.size(data)
    if type_ch=='ind':
        data_size=sc.size(data)
        mat_out=sc.empty((10,))
    else:
        data_size=sc.size(data[0,:])
        mat_out=sc.empty((10,data_size))
    recap_out=sc.empty((4,))
    if unit.find('dB')==0 or unit.find('DB')==0 or unit.find('db')==0:# permet de faire le calcul si les utnités sont en dB
        if type_ch=='ind':
            temp=sc.power(10,abs(data)/float(20))
            mat_out[0]=20*sc.log10(sc.mean(temp))
            mat_out[1]=20*sc.log10(sc.std(temp))
            mat_out[2]=20*sc.log10(sc.mean(temp)-sc.std(temp)/(sc.sqrt(n)))
            mat_out[3]=20*sc.log10(sc.mean(temp)+sc.std(temp)/(sc.sqrt(n)))
            mat_out[4]=20*sc.log10(sc.mean(temp)-2* sc.std(temp)/(sc.sqrt(n)))
            mat_out[5]=20*sc.log10(sc.mean(temp)+2*sc.std(temp)/(sc.sqrt(n)))
            mat_out[6]=20*sc.log10(sc.mean(temp)-3* sc.std(temp)/(sc.sqrt(n)))
            mat_out[7]=20*sc.log10(sc.mean(temp)+3*sc.std(temp)/(sc.sqrt(n)))
            mat_out[8]=20*sc.log10(sc.amax(temp))
            mat_out[9]=20*sc.log10(sc.amin(temp)) 
            #############################################
            recap_out[0]=mat_out[0]
            recap_out[1]=mat_out[1]
            recap_out[2]=mat_out[8]
            recap_out[3]=mat_out[9]
        else:
            temp=sc.power(10,abs(data[:,:])/float(20))
            mat_out[0,:]=20*sc.log10(sc.mean(temp,axis=0))
            mat_out[1,:]=20*sc.log10(sc.std(temp,axis=0))
            mat_out[2,:]=20*sc.log10(sc.mean(temp,axis=0)-sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[3,:]=20*sc.log10(sc.mean(temp,axis=0)+sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[4,:]=20*sc.log10(sc.mean(temp,axis=0)-2* sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[5,:]=20*sc.log10(sc.mean(temp,axis=0)+2*sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[6,:]=20*sc.log10(sc.mean(temp,axis=0)-3* sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[7,:]=20*sc.log10(sc.mean(temp,axis=0)+3*sc.std(temp,axis=0)/(sc.sqrt(n)))
            mat_out[8,:]=20*sc.log10(sc.amax(temp,axis=0))
            mat_out[9,:]=20*sc.log10(sc.amin(temp,axis=0)) 
            ################################################
            recap_out[0]=20*sc.log10(sc.mean(sc.mean(temp,axis=0)))
            recap_out[1]=20*sc.log10(sc.mean(sc.std(temp,axis=0)))
            recap_out[2]=sc.amax(20*sc.log10(sc.amax(temp,axis=0)))
            recap_out[3]=sc.amin(20*sc.log10(sc.amin(temp,axis=0)))
    ######################################################
    else:# si pas en dB   
        if type_ch=='ind':
            mat_out[0]=sc.mean(data)
            mat_out[1]=sc.std(data)
            mat_out[2]=sc.mean(data)-mat_out[1]/(sc.sqrt(n))
            mat_out[3]=sc.mean(data)+mat_out[1]/(sc.sqrt(n))
            mat_out[4]=sc.mean(data)-2* mat_out[1]/(sc.sqrt(n))
            mat_out[5]=sc.mean(data)+2*mat_out[1]/(sc.sqrt(n))
            mat_out[6]=sc.mean(data)-3* mat_out[1]/(sc.sqrt(n))
            mat_out[7]=sc.mean(data)+3*mat_out[1]/(sc.sqrt(n))
            mat_out[8]=sc.amax(data,axis=0)
            mat_out[9]=sc.amin(data,axis=0)
        else:
            mat_out[0,:]=sc.mean(data[:,:],axis=0)
            mat_out[1,:]=sc.std(data[:,:],axis=0)
            if type_ch=='freq':############# assure un intervalle de confiance non-negatif pour les spectres 
                mat_out[2,:]=sc.mean(data[:,:],axis=0)-mat_out[1,:]/(sc.sqrt(n))
                mat_out[2,sc.where(mat_out[2,:]<1e-12)]=1e-12
                mat_out[3,:]=sc.mean(data[:,:],axis=0)+mat_out[1,:]/(sc.sqrt(n))
                mat_out[3,sc.where(mat_out[3,:]<1e-12)]=1e-12
                mat_out[4,:]=sc.mean(data[:,:],axis=0)-2* mat_out[1,:]/(sc.sqrt(n))
                mat_out[4,sc.where(mat_out[4,:]<1e-12)]=1e-12
                mat_out[5,:]=sc.mean(data[:,:],axis=0)+2*mat_out[1,:]/(sc.sqrt(n))
                mat_out[5,sc.where(mat_out[5,:]<1e-12)]=1e-12
                mat_out[6,:]=sc.mean(data[:,:],axis=0)-3* mat_out[1,:]/(sc.sqrt(n))
                mat_out[6,sc.where(mat_out[6,:]<1e-12)]=1e-12
                mat_out[7,:]=sc.mean(data[:,:],axis=0)+3*mat_out[1,:]/(sc.sqrt(n))
                mat_out[7,sc.where(mat_out[7,:]<1e-12)]=1e-12
            else:
                mat_out[2,:]=sc.mean(data[:,:],axis=0)-mat_out[1,:]/(sc.sqrt(n))
                mat_out[3,:]=sc.mean(data[:,:],axis=0)+mat_out[1,:]/(sc.sqrt(n))
                mat_out[4,:]=sc.mean(data[:,:],axis=0)-2* mat_out[1,:]/(sc.sqrt(n))
                mat_out[5,:]=sc.mean(data[:,:],axis=0)+2*mat_out[1,:]/(sc.sqrt(n))
                mat_out[6,:]=sc.mean(data[:,:],axis=0)-3* mat_out[1,:]/(sc.sqrt(n))
                mat_out[7,:]=sc.mean(data[:,:],axis=0)+3*mat_out[1,:]/(sc.sqrt(n))
            mat_out[8,:]=sc.amax(data[:,:],axis=0)
            mat_out[9,:]=sc.amin(data[:,:],axis=0)
            ########################################################
            recap_out[0]=sc.mean(mat_out[0,:])
            recap_out[1]=sc.mean(mat_out[1,:])
            recap_out[2]=sc.amax(mat_out[8,:])
            recap_out[3]=sc.amin(mat_out[9,:])
        ###########################################
    return mat_out,recap_out

class main_statistique(QtGui.QMainWindow, stat_douai_GUI3.Ui_MainWindow):# MAIN du programme
    def __init__(self, parent=None):
        super(main_statistique, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Statistique Bruyance") 
        self.variable=Variable()
        self.progress_bar=calcul_bar()
        self.clear_button.setEnabled(False)
        self.fig_button.setEnabled(False)
        ######### initialise les listes et les types de sélection ################
        self.list_data.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.list_file.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.list_file.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
#        self.downsample_cb.addItems(['1','2','5','10'])
#        self.downsample_cb.setCurrentIndex(3)
        ######################################################
        self.connectActions()

    def connectActions(self):# gere les actions type bouton, selection de liste
        self.add_button.clicked.connect(self.load_file)
        self.calcul_button.clicked.connect(self.calcul_stat)
        self.fig_button.clicked.connect(self.add_graph)
        self.clear_button.clicked.connect(self.clean_list)
        self.list_file.itemDoubleClicked.connect(self.remove_item)
        
    def remove_item(self):# supprime les nom de fichiers selectionnés
        item = self.list_file.takeItem(self.list_file.currentRow())
        item = None
        
    def clean_list(self):# nettoie les listes
        self.clear_button.setEnabled(False)
        self.fig_button.setEnabled(False)
        self.variable.reset()
        self.list_file.clear()
        self.info_edit.clear()
                
    def load_file(self):# recupère les fichiers a utiliser et verifie qu'ils sortent de la meme configuration et extrait les noms des variables
        self.clear_button.setEnabled(True)
        lib.DWInit()# intialise la library dewesoft
        path_file=QtGui.QFileDialog.getOpenFileNames(self,'choisir des fichiers',"", "DXD files (*.dxd)")# ouvre une fenetre de dialogue pour choisir les fichiers
        path_file=path_file[0]
        flag=[]
        for ii in range(0,len(path_file)):# ouvre tout les fichiers
            ind=sc.where(self.variable.nom_fichier==path_file[ii])
            if len(ind[0])==0:#recupère les bons noms
                name=path_file[ii]
                name=name.replace('/','\\')
                name=name.split('\\')
                #######################################################
                # premier fichier, recupération des noms des variables et affichage
                ######################################################
                if self.list_file.count()==0:# si premier fichier , on récupère le sinfos de la config
                    self.variable.nom_fichier=sc.append(self.variable.nom_fichier,path_file[ii])
                    self.list_file.addItem(name[-1])# affiche le nom dans la liste des fichier
                    ####### lecture d ela config du premier fichier
                    self.config_init=CONFIG_DXD()# creer uen classe config
                    self.config_init=get_DXD_config(self.variable.nom_fichier[ii])# recupère la config
                    self.variable.point_temp=sc.linspace(0,self.config_init.duration,int(sc.amax(self.config_init.sample_channel)))# calcul de la base temps qui sera utilisée pour le traitemen
                    self.variable.point_freq=sc.linspace(0,sc.amax(self.config_init.sample_rate)/2,int(sc.amax(self.config_init.size_channel)))# calcul la base freq
                    for kk in range(0,self.config_init.num_channel):# ajoute les voies et les infos des voies à la list_data
                        item=QtGui.QTableWidgetItem()
                        item.setText(self.config_init.name_channel[kk])
                        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
                        item.setCheckState(QtCore.Qt.Unchecked)   
                        #################################################
                        max_row=self.list_data.rowCount()
                        self.list_data.insertRow(max_row)
                        self.list_data.setItem(max_row,0,item)
                        self.list_data.setItem(max_row,1,QtGui.QTableWidgetItem(self.config_init.unit_channel[kk]))
                        self.list_data.setItem(max_row,2,QtGui.QTableWidgetItem(self.config_init.space_channel[kk]))
                        self.list_data.setItem(max_row,3,QtGui.QTableWidgetItem(str(max(self.config_init.sample_channel[kk],self.config_init.size_channel[kk]))))
                else:
                    config_temp=CONFIG_DXD()
                    config_temp=get_DXD_config(path_file[ii])
                    flag2=False
                    try:
                        for kk in range(0,self.config_init.num_channel):
                            if config_temp.name_channel[kk]!=self.config_init.name_channel[kk]:
                                flag2=True
                    except:
                        flag2=True
                        pass
                    if flag2==True:# si fichier non lu, on ajoute son nom au vecteur flag
                        flag.append(name[-1])
                    elif config_temp.sample_rate==self.config_init.sample_rate:
                        self.variable.nom_fichier=sc.append(self.variable.nom_fichier,path_file[ii])
                        self.list_file.addItem(name[-1])
                    else:
                        flag.append(name[-1])
                if len(flag)!=0:# si flag contient des noms, on indique à l'opérateur que ces fichiers n'ont pas pu être ouverts car ils ont des config différentes
                    self.info_edit.clear()
                    self.info_edit.insert('Le(s) fichier(s):'+str(flag)+'ne peuvent être ouverts ou leurs configurations sont différentes')
                
#        lib.DWDeInit()

    def calcul_stat(self):# si bouton calcul cliqué--> on récupère les voies sélectionnées de tout les fichiers, on les enregistre et on calcul les données statisitques
        lib.DWInit()
        self.variable.list_data_index=[]
        ######################### recuperere les variables à traiter ################
        for ii in range(0,self.list_data.rowCount()):
            if self.list_data.item(ii,0).checkState()==QtCore.Qt.Checked:
                 self.variable.list_data_index.append(ii)
        #######################################################
        #  Recupere le nom et le type des variables a traiter
        #########################################################
        self.data=DATA()# definit une variable DATA stockant toutes les données
        ############ on récupère les inices des voies sélectionnées suivan tleur type afin de sles extraire facilement par la suite ###########
        ############ voies temporelles ####################
        ind=sc.where(self.config_init.space_channel[self.variable.list_data_index]=='Temps')
        ind=sc.array(ind).astype('int')[0]
        for jj in range(0,len(ind)):
            self.data.indice_temp.append(self.variable.list_data_index[ind[jj]])
        nb_temp=sc.size(self.data.indice_temp)
        #################################################
        ############ voies frequentielles ####################
        ind=sc.where(self.config_init.space_channel[self.variable.list_data_index]=='Fréquence')
        ind=sc.array(ind).astype('int')[0]
        for jj in range(0,len(ind)):
            self.data.indice_freq.append(self.variable.list_data_index[ind[jj]])
        nb_freq=sc.size(self.data.indice_freq)
        ################################################
        ############ voies indicateurs ####################
        ind=sc.where(self.config_init.space_channel[self.variable.list_data_index]=='Indicateur')
        ind=sc.array(ind).astype('int')[0]
        for jj in range(0,len(ind)):
            self.data.indice_ind.append(self.variable.list_data_index[ind[jj]])
        nb_indicator=sc.size(self.data.indice_ind)
        ##################  declares les matrices contenant les données des voies #######################################
        self.data.data_temp=sc.empty((sc.size(self.variable.nom_fichier),sc.size(self.variable.point_temp),nb_temp))
        self.data.data_freq=sc.empty((sc.size(self.variable.nom_fichier),sc.size(self.variable.point_freq),nb_freq))
        self.data.data_indicator=sc.empty((sc.size(self.variable.nom_fichier),nb_indicator ))
        ########################################
        #ouverture de la barre de progression
        ########################################
        self.progress_bar.show()
        self.progress_bar.progession_bar.setValue(0)
        ########################################
        for kk in range(0,sc.size(self.variable.nom_fichier)):
            self.progress_bar.progession_bar.setValue(int(100*(kk/sc.size(self.variable.nom_fichier))))
            app.processEvents()
            tt=0
            ff=0
            ii=0
            unit_temp=[]
            unit_freq=[]
            unit_ind=[]
            #################### enregistre les data dasn les matrices approrié et on rempli un vecteur de connectivité #######
            for jj in range(0,sc.size(self.variable.list_data_index)):# 
                [X,temp_data]=get_data(self.variable.nom_fichier[kk],self.variable.list_data_index[jj],self.config_init)# recupère les données et sotck dans temp_data
                ###########################################################################
                if self.config_init.space_channel[self.variable.list_data_index[jj]]=='Temps':# test pour savoir si voie temporel et recupère l'unité de la voie
                    unit_temp.append(self.config_init.unit_channel[self.variable.list_data_index[jj]])
                    if self.variable.point_temp.all()==X.all():
                        self.data.data_temp[kk,:,tt]=temp_data
                    else:
                        self.data.data_temp[kk,:,tt]=sc.interp(self.variable.point_temp,X,temp_data)
                    tt=tt+1
            #############################################################################
                if self.config_init.space_channel[self.variable.list_data_index[jj]]=='Fréquence':# test pour savoir si voie frequentiel et recupère l'unité de la voie
                    unit_freq.append(self.config_init.unit_channel[self.variable.list_data_index[jj]])
                    if self.variable.point_freq.all()==X.all():
                        self.data.data_freq[kk,:,ff]=temp_data
                    else:
                        self.data.data_freq[kk,:,ff]=sc.interp(self.variable.point_freq,X,temp_data)
                    ff=ff+1
            #################################################################################
                if self.config_init.space_channel[self.variable.list_data_index[jj]]=='Indicateur':# test pour savoir si voie indicateur et recupère l'unité de la voie
                    unit_ind.append(self.config_init.unit_channel[self.variable.list_data_index[jj]])
                    self.data.data_indicator[kk,ii]=temp_data[0]
                    ii=ii+1
        #####################################################################################
        # calcul des moyennes, ecart-type, intervalle de confiance
        #####################################################################################
        try:
            self.data.recap_temp=sc.empty((4,sc.size(self.data.data_temp[0,0,:])))
            self.data.stat_temp=sc.empty((10,sc.size(self.data.data_temp[0,:,0]),sc.size(self.data.data_temp[0,0,:])))
            for jj in range(0,sc.size(self.data.data_temp[0,0,:])):
                unit=unit_temp[jj]
                self.data.stat_temp[:,:,jj],self.data.recap_temp[:,jj]=statistique(self.data.data_temp[:,:,jj],unit,'temps')
        except:
            pass
        try:
            self.data.recap_freq=sc.empty((4,sc.size(self.data.data_freq[0,0,:])))
            self.data.stat_freq=sc.empty((10,sc.size(self.data.data_freq[0,:,0]),sc.size(self.data.data_freq[0,0,:])))
            for jj in range(0,sc.size(self.data.data_freq[0,0,:])):
                unit=unit_freq[jj]
                self.data.stat_freq[:,:,jj],self.data.recap_freq[:,jj]=statistique(self.data.data_freq[:,:,jj],unit,'freq')
        except:
                pass
        try:
            self.data.recap_indicator=sc.empty((4,sc.size(self.data.data_indicator[0,:])))
            self.data.stat_indicator=sc.empty((10,sc.size(self.data.data_indicator[0,:])))
            for jj in range(0,sc.size(self.data.data_indicator[0,:])):
                unit=unit_ind[jj]
                self.data.stat_indicator[:,jj],self.data.recap_indicator[:,jj]=statistique(self.data.data_indicator[:,jj],unit,'ind')
        except:
            pass
        self.progress_bar.close()
        self.fig_button.setEnabled(True)
#        lib.DWDeInit()  
        
    def add_graph(self):
        self.variable.graph_object.append(graphique(app))
        self.variable.graph_object[-1].open_graph(self.config_init,self.data,self.variable)
        
if __name__=='__main__':
    import sys
    global app
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    splash=splashScreen()
    splash.show_splash()  
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    gui = main_statistique()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-  
