#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Yorick Buot de l'Epine
# Update 02/2020 : Emmanuel Leguet
#
# Synthtone is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published by
# the Free Software Foundation.


###############################################################################
################################### Imports ###################################
###############################################################################

import sys
import os
import os.path
import psutil

import scipy
from scipy import signal

from PyQt5 import QtGui,QtCore # graphical interface
os.environ['ETS_TOOLKIT'] = 'qt5'
os.environ['QT_API'] = 'pyqt'
import pyqtgraph as pg # graphs
import pyaudio # audio
import wave # save the signal

# Files edited with Qt Designer and converted from .ui to .py
import main_gui
import param_gui
import filter_gui
import error_gui



###############################################################################
############################## Special functions ##############################
###############################################################################

# To find the path to additional files after conversion to .exe
def resource_path(relative):
    '''Get absolute path to resource, works for dev and for PyInstaller'''
    return os.path.join(os.environ.get("_MEIPASS2",os.path.abspath(".")),relative)


def kill_proc_tree(pid, including_parent=True):
    '''Close the program'''
    parent = psutil.Process(pid)
    for child in parent.children(recursive=True):
        child.kill()
    if including_parent:
        parent.kill()



###############################################################################
############################### Initial Values ################################
###############################################################################
        
##### General parameters #####
time_init = 2
sampling_init = 8192
##### Partiel creation #####
freq_init = 100
amp_init = 1
phase_init = 0
damp_init = 0.005
freq_max_init = 200
sweep_init = 0.001
##### Signal effects #####
noise_init = 0
AM_freq_init = 1000
AM_amp_init = 1
FM_freq_init = 1000
FM_amp_init = 1
FM_sensi_init = 0.5
##### Filters #####
freq1_filter_init = 100
freq2_filter_init = 1000
order_filter_init = 2
##### Hanning window #####
recouvrement_init = 0.66



###############################################################################
################### Functions to define the color table #######################
###############################################################################

def color_table():
    pos = scipy.array([0.0, 0.5, 1.0])
    color = scipy.array([[26,82,118,255], [20,143,119,255], [241,196,15,255]], dtype=scipy.ubyte)
    map = pg.ColorMap(pos, color)
    lut = map.getLookupTable(0.0, 1.0, 256)
    return lut

def colormap_gui(fig,min_val,max_val):
    nb_color=255
    value=scipy.array([scipy.linspace(min_val,max_val,nb_color),scipy.linspace(min_val,max_val,nb_color)])
    colortable=color_table()
    fig.clear()
    pen=pg.mkPen(color='k', width=2)
    image=pg.ImageItem()
    item_color=fig.getPlotItem()
    image.setLookupTable(colortable)
    fig.setBackground('w')
    fig.getPlotItem().hideButtons()
    fig.getPlotItem().setMouseEnabled(x=False,y=False)
    fig.addItem(image)
    image.setImage(value,autoDownSample=True)
    image.setLevels([min_val,max_val])
    Yaxis=item_color.getAxis('left')
    pen=pg.mkPen(color='k', width=2)
    fig.plotItem.getAxis('left').setPen(pen)
    fig.plotItem.getAxis('bottom').setPen(pen)
    ntick=7
    Ytick_loc=[]
    Ytick_string=[]
    for ii in range(0,ntick+1):
        Ytick_string.append("{0:.1f}".format(round(min_val+(ii*((max_val-min_val)/ntick)))))
        Ytick_loc.append(round((ii*((nb_color)/ntick))))
    Ytick=zip(Ytick_loc,Ytick_string)
    Yaxis.setTicks([list(Ytick)])
    
    
    
###############################################################################
################################# Classes #####################################
###############################################################################
    
###################### Opening picture of the software ########################
class splashScreen:
    
    def __init__(self):
        self.splash = QtGui.QWidget()
        splash_pix = QtGui.QPixmap(os.getcwd()+'\splash.png')
#        splash_pix = QtGui.QPixmap(resource_path('D:\\Users\\Utilisateur\\Desktop\\SOFT_PARTIEL\\splash.png'))
        self.splash.setFixedSize(splash_pix.size())
        self.splash.setWindowFlags(QtCore.Qt.FramelessWindowHint | QtCore.Qt.WindowStaysOnTopHint)
        palette = QtGui.QPalette()
        palette.setBrush(10, QtGui.QBrush(splash_pix)) # 10 = Windowrole
        self.splash.setPalette(palette)

    def show_splash(self):
        self.splash.show()
        
    def close_splash(self):
        self.splash.close()    
    
    
############################ Initialize variables #############################
class Variable:
    
    def __init__(self):
        self.freq_max = [] # max frequency to study
        self.buffer_size = [] # size of the buffer
        self.delta_freq=[]
        self.window=[] # windowed type
        self.covering=[] # covering for windowed
        self.channel=[]
        self.time_max=[]
        self.data_time=[]
        self.spectro_time=[]
        self.data_freq=[]
        self.nb_cover=[]
        self.db_min=[]
        self.db_max=[]
        self.factor_rate=2
        self.enveloppe_X=[]
        self.enveloppe_Y=[]


############################## Partiel creation ################################
class PARTIEL:
    
    def _init_(self):
        self.freq=[]
        self.amp=[]
        self.phase=[]
        self.tau=[] # damping


############################### Apply filters #################################
class FILTER:
    
    def _init_(self):
        self.type=[]
        self.freq1=[]
        self.freq2=[]
        self.order=[]


############################### Filter Window #################################
class filter_window(QtGui.QWidget,filter_gui.Ui_filter):
    
    def __init__(self, parent=None):
        super(filter_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Filter View")


############################ Parameters Window ################################
class param_window(QtGui.QWidget,param_gui.Ui_param):
    
    def __init__(self, parent=None):
        super(param_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Parameter")


############################### Errors Window #################################
class error_window(QtGui.QWidget,error_gui.Ui_error):
    
    def __init__(self, parent=None):
        super(error_window, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"Error")
        self.connectActions()
        
    def connectActions(self):
        self.ok_button.clicked.connect(self.error_close)
        
    def display_error(self,error_msg):
        self.err=error_window()
        self.err.error_text.setText(error_msg)
        self.err.show()
        
    def error_close(self):
        self.close()


######################### Plot the temporal signal ############################
class plot_sig:
    
    def __init__(self,p,var):
        pen=pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground('w')
        self.courbe=self.p.plot()
        self.p.disableAutoRange()
        self.p.setClipToView(True)
        self.p.setDownsampling(ds=5, auto=True, mode='peak')
        self.courbe.setData(scipy.linspace(0,var.time_max,50),scipy.zeros((50)),pen=pg.mkPen('b',width=2))
        self.env=self.p.plot()
        self.env.setData(scipy.linspace(0,var.time_max,50),scipy.zeros((50)),pen=pg.mkPen('r',width=2))
        self.p.setXRange(0,50)
        self.p.setYRange(-1.05,1.05)
        
        ##### axis shaping #####
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        self.p.plotItem.showGrid(x=True,y=True)
        self.p.setLabel('left', text ='Amplitude (V)')
        self.p.setLabel('bottom',text ='Time (s)')
        
    def update_data(self,X,Y):
        self.courbe.setData(X,Y,pen=pg.mkPen(color='b',width=0.9))
        self.p.setXRange(0,X[-1])
        self.p.setYRange(-1.05,1.05)
        
    def update_env(self,X,Y):
        self.env.setData(X,Y,pen=pg.mkPen(color='r',width=0.9))
        self.p.setXRange(0,X[-1])
        self.p.setYRange(-1.05,1.05)
        
    def suppr(self):
        self.p.clear()


################### Plot the temporal/frequency spectrum ######################
class plot_spectrum:
    
    def __init__(self,p,var):
        pen=pg.mkPen(color='k', width=2)
        self.p = p
        self.p.setBackground('w')
        self.p.getPlotItem().hideButtons()
        self.p.getPlotItem().setMouseEnabled(x=False,y=False)
        colortable=color_table()
        self.image=pg.ImageItem()
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        
        ##### define ticks #####
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        Xaxis.enableAutoSIPrefix(False)
        Xaxis.enableAutoSIPrefix(False)
        ntick=8.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.2f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(ii*scipy.size(var.data_freq)/ntick)
            Xtick_string.append("{0:.2f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(ii*var.spectro_time/ntick)
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick=zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        self.p.setLabel('left', text ='<font size="4">Frequency </font>', units ='<font size="4">Hz </font>')
        self.p.setLabel('bottom',text ='<font size="4">Time </font>', units ='<font size="4">Seconds </font>')


    def reset(self,var):
        pen=pg.mkPen(color='k', width=2)
        self.p.removeItem(self.image)
        self.p.removeItem(self.p.getPlotItem().getAxis('bottom'))
        self.p.removeItem(self.p.getPlotItem().getAxis('left'))
        colortable=color_table()
        self.image=pg.ImageItem()
        self.image.setLookupTable(colortable)
        self.p.addItem(self.image)
        self.p.plotItem.getAxis('left').setPen(pen)
        self.p.plotItem.getAxis('bottom').setPen(pen)
        
        ##### define ticks #####
        item_spectro=self.p.getPlotItem()
        Xaxis=item_spectro.getAxis('bottom')
        Yaxis=item_spectro.getAxis('left')
        ntick=8.0
        Xtick_loc=[]
        Xtick_string=[]
        Ytick_loc=[]
        Ytick_string=[]
        for ii in range(0,int(ntick+1)):
            Ytick_string.append("{0:.2f}".format(ii*var.freq_max/ntick))
            Ytick_loc.append(ii*scipy.size(var.data_freq)/ntick)
            Xtick_string.append("{0:.2f}".format(ii*var.time_max/ntick))
            Xtick_loc.append(ii*var.spectro_time/ntick)
        Xtick = zip(Xtick_loc,Xtick_string)
        Ytick = zip(Ytick_loc,Ytick_string)
        Xaxis.setTicks([list(Xtick)])
        Yaxis.setTicks([list(Ytick)])
        
        #####  put the image back in the background #####
        self.image.setZValue(-1) 

    def update_data(self,mat,db_min,db_max):
         self.image.setImage(mat)
         self.image.setLevels([db_min,db_max])

      
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# # # # # # # # # # # # # # # # MAIN CLASS # # # # # # # # # # # # # # # # # #
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #  
class spectro(QtGui.QMainWindow,main_gui.Ui_MainWindow):
    
    def __init__(self, parent=None):
        super(spectro, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle(u"SynthTone")
#        self.setWindowIcon(QtGui.QIcon(resource_path('D:\\Users\\Utilisateur\\Desktop\\SOFT_PARTIEL\\logo_synthtone.ico')))
        self.setWindowIcon(QtGui.QIcon(os.getcwd()+'\logo_synthtone.ico'))
        
        #####  define the parameters window #####
        self.param = param_window()
        
        #####  initialize variable #####
        os.chdir('C:\\')
        self.variable = Variable()
        self.acqui=False
        
        ##### time & sampling #####
        self.time_edit.insert(str(time_init))
        self.sampling_edit.insert(str(sampling_init))
        
        ##### amplitude modulation #####
        self.AM_freq_edit.insert(str(AM_freq_init))
        self.AM_amp_edit.insert(str(AM_amp_init))
        
        ##### frequence modulation #####
        self.FM_freq_edit.insert(str(FM_freq_init))
        self.FM_amp_edit.insert(str(FM_amp_init))
        self.FM_sensi_edit.insert(str(FM_sensi_init))
        
        ##### sweep #####
        self.sweep_cb.addItems(['Lin','Exp'])
        self.sweep_cb.setCurrentIndex(0)
        
        ##### noise #####
        self.noise_edit.insert(str(noise_init))
        
        ##### dB values #####
        self.db_min_edit.insert('-140')
        self.db_max_edit.insert('0')
        
        ##### parameters window #####
        self.variable.freq_max = float(self.sampling_edit.text())/float(self.variable.factor_rate)
        self.param.bandwidth_label.setText(str(self.variable.freq_max))
        self.param.line_box.addItems(['128','256','512','1024','2048','4096'])
        self.param.line_box.setCurrentIndex(2)
        self.variable.buffer_size = int(self.variable.factor_rate*int(self.param.line_box.currentText()))
        self.param.fenetre_box.addItems(['Uniform','Hanning'])
        self.param.fenetre_box.setCurrentIndex(1)
        self.variable.window = self.param.fenetre_box.currentText()  # windowed type
        self.param.recouvrement_edit.insert(str(recouvrement_init))
        self.variable.covering = float(self.param.recouvrement_edit.text()) # covering for windowed
        self.define_delta_f()
        
        ##### initial plot #####
        self.define_colormap()
        self.define_data_freq_time()
        self.signal_plot=plot_sig(self.plot_signal,self.variable)
        self.spectro_plot=plot_spectrum(self.plot_spectrum,self.variable)
        
        ##### define the enveloppe #####
        pos=scipy.linspace(0,self.variable.time_max,5)
        val_h=[1,1,1,0.5,0]
        handle=zip(pos,val_h)
        self.enveloppe_roi=pg.PolyLineROI(handle,closed=False,movable=False,pen=pg.mkPen('g',width=0.9))
        self.enveloppe_roi.handlePen=pg.mkPen('b')
        hand=self.enveloppe_roi.getHandles()
        for ii in range(0,scipy.size(hand)):
            hand[ii].pen=pg.mkPen('b')
            hand[ii].currentPen=pg.mkPen('b')
        self.signal_plot.p.addItem(self.enveloppe_roi)
        self.modify_enveloppe()
        
        ##### define the tables #####
        self.partiel_table.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.partiel_list=[]
        self.filter_table.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        self.filter_liste=[]
        self.liste_filter_button=[]
        
        ##### #####
        self.listen_button.setEnabled(False)
        self.save_button.setEnabled(False)
        self.connectActions()


    def connectActions(self):
        ''' Action triggered to another action'''
        ##### parameters window #####
        self.param_button.clicked.connect(self.param_open)
        self.param.enregistrement_button.clicked.connect(self.save_param)
        self.param.line_box.currentIndexChanged.connect(self.define_delta_f)
        self.param.fenetre_box.currentIndexChanged.connect(self.define_window)
        ##### others #####
        self.add_freq_button.clicked.connect(self.add_freq)
        self.add_filter_button.clicked.connect(self.add_filter)
        self.listen_button.clicked.connect(self.listen_sig)
        self.db_max_edit.editingFinished.connect(self.define_colormap)
        self.db_min_edit.editingFinished.connect(self.define_colormap)
        self.sampling_edit.editingFinished.connect(self.actualise_param)
        self.sampling_edit.editingFinished.connect(self.define_delta_f)
        self.time_edit.editingFinished.connect(self.actualise_param)
        self.clean_partiel_button.clicked.connect(self.clean_partiel)
        self.clean_filter_button.clicked.connect(self.clean_filter)
        self.enveloppe_box.stateChanged.connect(self.modify_enveloppe)
        self.save_button.clicked.connect(self.save_wave)
        self.sweep_box.clicked.connect(self.on_off_sweep_param)
        ##### Launch the main function #####
        self.create_button.clicked.connect(self.main)
        
        
        
    def on_off_sweep_param(self):
        ''' Activate/Desactivate the editable case for the sweep'''
        row=self.partiel_table.rowCount()
        # Allow to edit the values of the slope and the second frequence
        if self.sweep_box.isChecked()==True:
            for i in range(0,row):
                self.partiel_table.item(i,5).setFlags( QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )
                self.partiel_table.item(i,6).setFlags( QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )
                
        # Prevent from editing the values of the slope and the second frequence 
        else:
           for i in range(0,row):
                self.partiel_table.item(i,5).setFlags( QtCore.Qt.ItemIsSelectable )
                self.partiel_table.item(i,6).setFlags( QtCore.Qt.ItemIsSelectable )
    
    
    def param_open(self):
        """Lance la deuxième fenêtre"""
        self.param.show()


    def save_param(self):
        ''' Save the data of the parameters window'''
        self.variable.buffer_size = int(self.variable.factor_rate*int(self.param.line_box.currentText())) # size of buffer
        self.variable.window = self.param.fenetre_box.currentText()  # windowed type
        self.variable.covering = float(self.param.recouvrement_edit.text()) # covering for windowed
        self.define_data_freq_time()
        self.param.close()


    def define_data_freq_time(self):
        ''' Define the data necessary for the time/freq graph'''
        ##### overlap #####
        cover = float(self.variable.covering)
        if cover < 0 or cover > 1:
                cover = recouvrement_init
        ##### total time #####
        time_edit = float(self.time_edit.text())
        if time_edit <= 0:
            time_edit = time_init
        ##### sampling #####
        sampling_edit = float(self.sampling_edit.text())
        if sampling_edit < int(self.param.line_box.currentText()):
            sampling_edit = int(self.param.line_box.currentText())
            
        ##### define parameters #####
        delta_f = self.variable.delta_freq # equivalant to the number of buffers to fill per sec
        self.variable.nb_cover=int(round(1/(1-cover)))
        self.variable.data_freq=scipy.arange(0,self.variable.freq_max,delta_f) # [0:-1]
        self.variable.time_max=time_edit
        self.variable.data_time=scipy.arange(0,self.variable.time_max,1/float(self.variable.factor_rate*self.variable.freq_max))
        
        ##### Number of windows #####
        buffer_size = int(self.variable.factor_rate*int(self.param.line_box.currentText()))
        N = scipy.ceil(sampling_edit*self.variable.time_max)-buffer_size # Total of time points minus the first buffer
        Nshift = scipy.ceil((1-cover)*buffer_size) # Number of new time points include in a window
        self.variable.spectro_time = 1 + scipy.ceil(N/Nshift) # Number of windows
        
        ###########################
        # creation of the buffers #
        ###########################
        self.signal_buffer = 1e-12*scipy.ones(scipy.size(self.variable.data_time)) # We use 1e-12 or 1e-7 to replace 0 (issue in dB)
        ###### spectre sur toute la durrée de l'enregistrement + nouveau spectre generé #####
        self.spectro_buff = 1e-7*scipy.ones((scipy.size(self.variable.data_freq),scipy.size(self.variable.data_time)))
        
        
    def define_window(self):
        ''' Define the type of window to use'''
        fenetre=self.param.fenetre_box.currentText()
        if fenetre=='Uniform':
            self.param.recouvrement_edit.setText('0')
        if fenetre=='Hanning':
            self.param.recouvrement_edit.setText(str(recouvrement_init))


    def define_delta_f(self):
        ''' Define the informational frequency step for time/freq graph'''
        self.param.enregistrement_button.setEnabled(True)
        self.variable.delta_freq = int(self.variable.freq_max)/int(self.param.line_box.currentText())
        self.param.delta_label.setText("{0:.3f}".format(self.variable.delta_freq))


    def define_colormap(self):
        ''' Define the color map for the time/freq graph'''
        val_min = int(self.db_min_edit.text())
        val_max = int(self.db_max_edit.text())
        colormap_gui(self.colorbar_plot,val_min,val_max)
        self.variable.db_min = val_min
        self.variable.db_max = val_max
        ##### if new acquisition, we plot the new spectrum #####
        if self.acqui:
            self.spectro_plot.update_data(self.spectro_buff.T,self.variable.db_min,self.variable.db_max)


    def add_freq(self):
        ''' Add a new partiel'''
        max_row = self.partiel_table.rowCount()
        item = QtGui.QTableWidgetItem()
        item.setText('Partiel '+str(max_row))
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        item.setCheckState(QtCore.Qt.Checked)
        
        ##### initialization #####
        self.partiel_table.insertRow(max_row)
        self.partiel_table.setItem(max_row,0,item)
        self.partiel_table.setItem(max_row,1,QtGui.QTableWidgetItem(str(freq_init)))
        self.partiel_table.setItem(max_row,2,QtGui.QTableWidgetItem(str(amp_init)))
        self.partiel_table.setItem(max_row,3,QtGui.QTableWidgetItem(str(phase_init)))
        self.partiel_table.setItem(max_row,4,QtGui.QTableWidgetItem(str(damp_init)))
        self.partiel_table.setItem(max_row,5,QtGui.QTableWidgetItem(str(freq_max_init)))
        self.partiel_table.setItem(max_row,6,QtGui.QTableWidgetItem(str(sweep_init)))
        
        ##### sweep parameters #####
        # Allow to edit the values of the slope and the second frequence
        if self.sweep_box.isChecked()==True:
            self.partiel_table.item(max_row,5).setFlags( QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )
            self.partiel_table.item(max_row,6).setFlags( QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable | QtCore.Qt.ItemIsEnabled )
        # Prevent from editing the values of the slope and the second frequence 
        else:
            self.partiel_table.item(max_row,5).setFlags( QtCore.Qt.ItemIsSelectable )
            self.partiel_table.item(max_row,6).setFlags( QtCore.Qt.ItemIsSelectable )


    def add_filter(self):
        ''' Add a new filter'''
        max_row = self.filter_table.rowCount()
        item = QtGui.QTableWidgetItem()
        item.setText('Filter '+str(max_row))
        item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
        item.setCheckState(QtCore.Qt.Checked)
        
        ##### type box #####
        combo = QtGui.QComboBox()
        combo.addItems(['LowPass','HighPass','BandPass','BandStop'])
        combo.setCurrentIndex(1)
        
        ##### view box #####
        button=QtGui.QPushButton()
        button.setText('View')
        self.liste_filter_button.append(button)
        button.clicked.connect(lambda:self.view_filter(max_row))
        
        ##### initialization #####
        self.filter_table.insertRow(max_row)
        self.filter_table.setItem(max_row,0,item)
        self.filter_table.setCellWidget(max_row,1,combo)
        self.filter_table.setItem(max_row,2,QtGui.QTableWidgetItem(str(freq1_filter_init)))
        self.filter_table.setItem(max_row,3,QtGui.QTableWidgetItem(str(freq2_filter_init)))
        self.filter_table.setItem(max_row,4,QtGui.QTableWidgetItem(str(order_filter_init)))
        self.filter_table.setCellWidget(max_row,5,button)



    def modify_enveloppe(self):
        ''' Modify the enveloppe that shape the signal'''
        ##### enveloppe activated #####
        if self.enveloppe_box.isChecked():
            self.signal_plot.p.addItem(self.enveloppe_roi)
            self.signal_plot.p.addItem(self.signal_plot.env)
            hand = self.enveloppe_roi.getHandles() # should be improved
            self.variable.enveloppe_X=self.variable.data_time
            tin=[]
            valin=[]
            for ii in range(0,scipy.size(hand)):
                h=self.enveloppe_roi.getLocalHandlePositions(ii)
                pos=h[1]
                tin.append(float(pos.x()))
                valin.append(float(pos.y()))
                if valin[ii]<0:
                    valin[ii]=0
                if valin[ii]>1:
                    valin[ii]=1
            try: # Potentially the try isn't necessary anymore
                self.variable.enveloppe_Y = scipy.interp(self.variable.enveloppe_X,tin,valin)
                self.signal_plot.update_env(self.variable.enveloppe_X,self.variable.enveloppe_Y)
            except:
                pass
                
        ##### enveloppe desactivated #####
        else:
            self.signal_plot.p.removeItem(self.enveloppe_roi)
            self.signal_plot.p.removeItem(self.signal_plot.env)


    def actualise_param(self):
        ''' Refresh gereneral parameters'''
        ##### acquisition #####
        self.acqui = False
        ##### time #####
        time_edit = float(self.time_edit.text())
        if time_edit <= 0:
            time_edit = time_init
        self.variable.time_max=time_edit
        ##### sampling #####
        sampling_edit = float(self.sampling_edit.text())
        if sampling_edit < int(self.param.line_box.currentText()):
            sampling_edit = int(self.param.line_box.currentText())
        ##### others #####
        self.variable.freq_max = sampling_edit/self.variable.factor_rate
        self.param.bandwidth_label.setText("{0:.3f}".format(self.variable.freq_max))
        self.define_delta_f()
        self.define_data_freq_time()
        self.modify_enveloppe()
        self.define_colormap()
        self.listen_button.setEnabled(False)
        self.save_button.setEnabled(False)


    def view_filter(self,row):
        ''' Display a filter'''
        row=row
        self.filt_window=filter_window()
        self.filt_window.label_filter.setText('Filter')
        filter=FILTER()
        filter.type=self.filter_table.cellWidget(row,1).currentIndex()
        
        ##### frequence 1 #####
        if float(self.filter_table.item(row,2).text())>(self.variable.freq_max):
            self.filter_table.setItem(row,2,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
            filter.freq1=self.variable.freq_max
        else:
            filter.freq1=float(self.filter_table.item(row,2).text())
            
        ##### frequence 2 #####
        if float(self.filter_table.item(row,3).text())>(self.variable.freq_max):
            self.filter_table.setItem(row,3,QtGui.QTableWidgetItem(str(self.variable.freq_max)))
            filter.freq2=self.variable.freq_max
        else:
            filter.freq2=float(self.filter_table.item(row,3).text())
            
        ##### order #####
        if float(self.filter_table.item(row,4).text())>8:
            self.filter_table.setItem(row,4,QtGui.QTableWidgetItem(str(8)))
            filter.order=8
        else:
            filter.order=float(self.filter_table.item(row,4).text())
            
        ##### type #####
        if filter.type==0:
                [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'lowpass') # cut-off frequency based on Nquyst frequency i.e fmax/2
        if filter.type==1:
                [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'highpass')
        if filter.type==2:
                flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandpass')
        if filter.type==3:
                flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandstop')
        [w, h] = scipy.signal.freqz(C, D)
        
        ##### display #####
        pen=pg.mkPen(color='k', width=2)
        p = self.filt_window.plot_filter
        p.setBackground('w')
        courbe=p.plot()
        p.setClipToView(True)
        p.plotItem.getAxis('left').setPen(pen)
        p.plotItem.getAxis('bottom').setPen(pen)
        p.plotItem.showGrid(x=True,y=True)
        p.setLogMode(x=True,y=False)
        p.setLabel('left', text ='Amplitude (dB)')
        p.setLabel('bottom',text ='Frequency (Hz)')
        p.setYRange(-60,10)
        courbe.setData(w*(self.variable.freq_max*self.variable.factor_rate)/float(2*scipy.pi), 20*scipy.log10(abs(h)),pen=pg.mkPen(color='b',width=2))
        self.filt_window.show()


    def clean_partiel(self):
        ''' Remove all partiels'''
        while self.partiel_table.rowCount() > 0:
            self.partiel_table.removeRow(0)


    def clean_filter(self):
        ''' Remove all filters'''
        while self.filter_table.rowCount() > 0:
            self.filter_table.removeRow(0)

        #################
    ##### Main Function #####
        #################
    def main(self):
        ''' Launched when the "Create" button is pushed (connectedActions)''' 
        # We verify at least one partiel has been added
        row=self.partiel_table.rowCount()
        if row > 0:
            ##############
            # Initialize #
            ##############
            self.error = error_window()
            self.listen_button.setEnabled(True)
            self.save_button.setEnabled(True)
            self.define_data_freq_time()
            self.modify_enveloppe()
            self.acqui=True
            List_error = "" # Initialization of the error list (string)
            
            ######################################################
            # Reading of the partiels and creation of the signal #
            ######################################################
            for ii in range(0,row):
                if self.partiel_table.item(ii,0).checkState()==QtCore.Qt.Checked:
                    partiel=PARTIEL()
                    ###### frequency ######
                    partiel.freq=float(self.partiel_table.item(ii,1).text())
                    if partiel.freq <= 0:
                        partiel.freq = freq_init
                        self.partiel_table.item(ii,1).setText(str(partiel.freq))
                        List_error += "- Frequency n°"+str(ii)+" should be positive !\n\n"
                   ###### amplitude ######
                    partiel.amp=float(self.partiel_table.item(ii,2).text())
                    if partiel.amp < 0:
                        partiel.amp = 0
                        self.partiel_table.item(ii,2).setText(str(partiel.amp))
                        List_error += "- Amplitude n°"+str(ii)+" should be in [0,1] !\n\n"
                    elif partiel.amp > 1:
                        partiel.amp = 1
                        self.partiel_table.item(ii,2).setText(str(partiel.amp))
                        List_error += "- Amplitude n°"+str(ii)+" should be in [0,1] !\n\n"
                    ###### phase ######
                    if abs(float(self.partiel_table.item(ii,3).text()))>180:
                        partiel.phase=scipy.sign(float(self.partiel_table.item(ii,3).text()))*float(180)
                        self.partiel_table.setItem(ii,3,QtGui.QTableWidgetItem(str(partiel.phase)))
                        List_error += "- Phase n°"+str(ii)+" should be in [-180,180] !\n\n"
                    else:
                        partiel.phase=float(self.partiel_table.item(ii,3).text())*scipy.pi/float(180)
                    ###### damping ######
                    partiel.tau=float(self.partiel_table.item(ii,4).text())
                    if partiel.tau>1:
                        partiel.tau=1
                        self.partiel_table.setItem(ii,4,QtGui.QTableWidgetItem(str(partiel.tau)))
                        List_error += "- Damping n°"+str(ii)+" should be in [0,1] !\n\n"
                    elif partiel.tau<0:
                        partiel.tau=0
                        self.partiel_table.setItem(ii,4,QtGui.QTableWidgetItem(str(partiel.tau))) 
                        List_error += "- Damping n°"+str(ii)+" should be in [0,1] !\n\n"
                        
                    #####################################
                    # Consideration of the ramp (sweep) #
                    #####################################
                    lamb=2*scipy.pi*partiel.freq*partiel.tau/float(len(self.variable.data_time))
                    if self.sweep_box.isChecked()==True:
                        ###### max frequency ######
                        partiel.freq_max=float(self.partiel_table.item(ii,5).text())
                        if partiel.freq_max <= 0:
                            partiel.freq_max = freq_max_init
                            self.partiel_table.item(ii,5).setText(str(partiel.freq_max))
                            List_error += "- Ramp_Freq_max n°"+str(ii)+" should be positive !\n\n"
                        ###### slope ######
                        sweep_const=float(self.partiel_table.item(ii,6).text())
                        if sweep_const <= 0:
                            sweep_const=sweep_init
                            self.partiel_table.item(ii,6).setText(str(sweep_const))
                            List_error += "- Slope should n°"+str(ii)+" be positive !\n\n"
                        ###### signal periodization ######
                        Period = 1/sweep_const
                        Periodic_Time = scipy.zeros(scipy.size(self.variable.data_time))
                        tps, increment = 0, 0
                        while increment < scipy.size(self.variable.data_time):
                            Periodic_Time[increment] = self.variable.data_time[tps]
                            tps += 1
                            if tps < scipy.size(self.variable.data_time) and self.variable.data_time[tps] > Period:
                                tps = 0
                            increment += 1
                        ###### sweep type######
                        sweep_type=self.sweep_cb.currentIndex()
                        # https://en.wikipedia.org/wiki/Chirp
                        if sweep_type==0: # linear
                            sweep_C = float(sweep_const)*(partiel.freq_max-partiel.freq) # Chirpyness
                            sig_partiel = partiel.amp*scipy.exp(-scipy.arange(scipy.size(self.variable.data_time))*lamb)*scipy.cos(2*scipy.pi*(0.5*sweep_C*Periodic_Time**2+partiel.freq*Periodic_Time))
                        elif sweep_type==1: # exponential
                            sweep_K = (partiel.freq_max/partiel.freq)**sweep_const # Rate of exponential change
                            sig_partiel = partiel.amp*scipy.exp(-scipy.arange(scipy.size(Periodic_Time))*lamb)*scipy.cos(2*scipy.pi*partiel.freq*((sweep_K**Periodic_Time-1)/scipy.log(sweep_K)))
                    
                    ###### no sweep ######
                    else: 
                        sig_partiel=partiel.amp*scipy.cos(partiel.freq*2*scipy.pi*self.variable.data_time-partiel.phase)
                        sig_partiel=sig_partiel*scipy.exp(-scipy.arange(scipy.size(self.variable.data_time))*lamb)
                    self.signal_buffer=self.signal_buffer+sig_partiel
                
            #################################
            # Applying frequency modulation #
            #################################
            if self.FM_box.isChecked():
                ###### sensitivity ######
                FM_sensi = float(self.FM_sensi_edit.text())
                if FM_sensi < 0:
                            FM_sensi = FM_sensi_init
                            self.FM_sensi_edit.setText(str(FM_sensi_init))
                            List_error += "- Sensitivity for frequency modulation should be positive !\n\n"
                ###### frequency ######
                FM_freq = float(self.FM_freq_edit.text())
                if FM_freq <= 0:
                            FM_freq = FM_freq_init
                            self.FM_freq_edit.setText(str(FM_freq_init))
                            List_error += "- Frequency for frequency modulation should be positive !\n\n"
                ###### amplitude ######
                FM_amp = float(self.FM_amp_edit.text())
                if FM_amp < 0 or FM_amp > 1:
                       FM_amp = FM_amp_init
                       self.FM_amp_edit.setText(str(FM_amp_init)) 
                       List_error += "- Amplitude for frequency modulation should be in [0,1] !\n\n"
                ###### applying frequency modulation ######
                FM_signal = scipy.zeros(len(self.signal_buffer))
                for ii in range(0,len(FM_signal)):
                    integ = scipy.trapz(self.signal_buffer[0:ii])
                    FM_signal[ii] = FM_amp*scipy.cos(2*scipy.pi*FM_freq*self.variable.data_time[ii]+2*scipy.pi*FM_sensi*integ) # page 312 analog and digital signal processing
                self.signal_buffer = FM_signal
    
            #################################
            # Applying amplitude modulation #
            #################################
            if self.AM_box.isChecked():
                ###### amplitude ######
                AM_amp = float(self.AM_amp_edit.text())
                if AM_amp < 0 or AM_amp > 1:
                       AM_amp = AM_amp_init
                       self.AM_amp_edit.setText(str(AM_amp_init)) 
                       List_error += "- Amplitude for amplitude modulation should be in [0,1] !\n\n"
                ###### frequency ######
                AM_freq = float(self.AM_freq_edit.text())
                if AM_freq <= 0:
                            AM_freq = AM_freq_init
                            self.AM_freq_edit.setText(str(AM_freq_init))
                            List_error += "- Frequency for amplitude modulation should be positive !\n\n"
                ###### applying amplitude modulation ######    
                amp_max = scipy.amax(abs(self.signal_buffer))
                AM_index = amp_max/float(AM_amp)
                self.signal_buffer = AM_amp*scipy.cos(2*scipy.pi*self.variable.data_time*AM_freq)*(1+(AM_index/float(amp_max))*self.signal_buffer) # page 309 analog and digital signal processing
                
            ##########################
            # Applying the enveloppe #
            ##########################
            if self.enveloppe_box.isChecked()==True:
                self.signal_buffer=self.signal_buffer*self.variable.enveloppe_Y
                
            ################
            # Adding noise #
            ################
            if self.noise_box.isChecked():
                noise_level = float(self.noise_edit.text())
                if noise_level < 0 or noise_level > 1:
                       noise_level = noise_init
                       self.noise_edit.setText(str(noise_init)) 
                       List_error += "- Noise level for amplitude modulation should be in [0,1] !\n\n"
                noise = scipy.random.rand(scipy.size(self.variable.data_time))*noise_level
                self.signal_buffer = self.signal_buffer+noise
                
            ####################
            # Applying filters #
            ####################
            row=self.filter_table.rowCount()
            for ii in range(0,row):
                if self.filter_table.item(ii,0).checkState()==QtCore.Qt.Checked:
                    filter = FILTER()
                    filter.type = self.filter_table.cellWidget(ii,1).currentIndex()
                    ###### frequency 1 ######
                    freq1 = float(self.filter_table.item(ii,2).text())
                    if freq1 >= self.variable.freq_max:
                        self.filter_table.item(ii,2).setText(str(self.variable.freq_max/2))
                        filter.freq1 = self.variable.freq_max/2
                        List_error += "- Freq1 n°"+str(ii)+" for filter should be lower than "+str(self.variable.freq_max)+" !\n\n"
                    elif freq1 <= 0:
                            freq1 = freq1_filter_init
                            filter.freq1 = freq1
                            self.filter_table.item(ii,2).setText(str(freq1_filter_init))
                            List_error += "- Freq1 n°"+str(ii)+" for filter should be positive !\n\n"      
                    else:
                        filter.freq1 = freq1
                    ###### frequency 2 ######
                    freq2 = float(self.filter_table.item(ii,3).text())
                    if freq2 >= self.variable.freq_max:
                        self.filter_table.item(ii,3).setText(str(self.variable.freq_max/2))
                        filter.freq2=self.variable.freq_max/2
                        List_error += "- Freq2 n°"+str(ii)+" for filter should be lower than "+str(self.variable.freq_max)+" !\n\n"
                    elif freq2 <= 0:
                            freq2 = freq2_filter_init
                            filter.freq2 = freq2
                            self.filter_table.item(ii,3).setText(str(freq2_filter_init))
                            List_error += "- Freq2 n°"+str(ii)+" for filter should be positive !\n\n"
                    else:
                        filter.freq2 = freq2
                    ###### order ######
                    Order = int(float(self.filter_table.item(ii,4).text()))
                    if Order > 8 or Order < 1:
                        filter.order = order_filter_init
                        self.filter_table.setItem(ii,4,QtGui.QTableWidgetItem(str(order_filter_init)))
                        List_error += "- Order n°"+str(ii)+" for filter should be in [1,8] !\n\n"
                    else:
                        self.filter_table.setItem(ii,4,QtGui.QTableWidgetItem(str(Order)))
                        filter.order = Order
                    ###### type ######
                    if filter.type==0:
                        [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'lowpass')# frequence coupure  basée sur la fréquence de Nquyst soit fmax/2
                    if filter.type==1:
                        [C, D]= scipy.signal.butter(filter.order,filter.freq1/float(self.variable.freq_max),'highpass')
                    if filter.type==2:
                        flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                        fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                        [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandpass')
                    if filter.type==3:
                        flow=scipy.amin([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                        fhigh=scipy.amax([filter.freq1,filter.freq2])/float(self.variable.freq_max)
                        [C, D]= scipy.signal.butter(filter.order,[flow,fhigh],'bandstop')
                    ###### applying the filter ######
                    self.signal_buffer=scipy.signal.filtfilt(C,D,self.signal_buffer)
                    
            ###### dealing with a zero value issue ######
            self.signal_buffer[scipy.where(self.signal_buffer==0)] = 1e-12
            
            ###### normalization before applying the enveloppe ######
            if scipy.amax(abs(self.signal_buffer)) > 1:
                    self.signal_buffer=self.signal_buffer/scipy.amax(abs(self.signal_buffer))
                    
                
            #########################
            # Display of the signal #
            #########################
            self.signal_plot.update_data(self.variable.data_time,self.signal_buffer)
            
            ###########################
            # Display of the spectrum #
            ###########################
            nb_freq = self.variable.buffer_size
            signal_buffer_0_padding = scipy.copy(self.signal_buffer) # Copy of the signal, zeros will be added in the hanning case
            freq_sample = self.variable.freq_max*self.variable.factor_rate
            
            ###### overlap ######
            if self.variable.covering < 0 or self.variable.covering > 1:
                self.variable.covering = recouvrement_init
                self.param.recouvrement_edit.setText(str(recouvrement_init))
                List_error += "- Overlap in parameters should be in [0,1] !\n\n"
            overlap=self.variable.covering
            
            ###### window choice ######
            if self.variable.window=='Uniform':
                window = 'boxcar'
            elif self.variable.window=='Hanning':
                window = 'hann'
                
                # Adding zeros to the buffer_signal to calculate the spectrogram in hanning case
                Npoints = len(self.signal_buffer) - nb_freq
                Nshift = int(scipy.ceil((1-overlap)*nb_freq))
                nb_zeros =  Nshift*int(scipy.ceil(Npoints/Nshift))-Npoints
                signal_buffer_0_padding = scipy.array(list(signal_buffer_0_padding) + nb_zeros*[0])
            
            ###### creation of the spectrum ######
            f, t, Sxx = signal.spectrogram(signal_buffer_0_padding,freq_sample,window=window,nperseg=nb_freq,noverlap=int((overlap)*nb_freq),nfft=nb_freq, detrend='constant', return_onesided=True, scaling='spectrum', axis=-1, mode='magnitude')
            self.spectro_buff=20*scipy.log10(abs(2*Sxx))
            
            ###### display of the spectrum ######
            self.spectro_plot.reset(self.variable)
            self.spectro_plot.update_data(self.spectro_buff.T,self.variable.db_min,self.variable.db_max)
            
            ###############################
            # Display errors if there are #
            ###############################
            # The errors are already corrected, but we notice the user of them
            
            ###### time ######
            if float(self.time_edit.text()) <= 0:
                self.time_edit.setText(str(time_init))
                List_error += "- Time should be positive !\n\n"
            ###### sampling ######
            if float(self.sampling_edit.text()) < int(self.param.line_box.currentText()):
                self.sampling_edit.setText(self.param.line_box.currentText())
                List_error += "- Sampling frequency should superior or equal to FFTLines !\n\n"
            ###### errors list ######
            if len(List_error) > 0:
                self.error.display_error(List_error)
            
            ###### refresh ######
            app.processEvents()


    def listen_sig(self):
        '''Play the signal'''
        self.listen_button.setEnabled(False)
        app.processEvents()
        p=pyaudio.PyAudio()
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        stream = p.open(format=pyaudio.paFloat32,channels=2,rate=RATE,input=False,output=True,frames_per_buffer=1024)
        data=scipy.zeros((len(self.signal_buffer),2))
        data[:,0]=self.signal_buffer
        data[:,1]=self.signal_buffer
        stream.write(data.astype(scipy.float32).tostring())
        stream.close()
        p.terminate()
        self.listen_button.setEnabled(True)
        app.processEvents()

    def save_wave(self):
        '''Save the signal'''
        dataW=scipy.zeros((scipy.size(self.signal_buffer),2))
        dataW[:,0]=scipy.multiply(self.signal_buffer,32767).astype(scipy.int16)
        dataW[:,1]=scipy.multiply(self.signal_buffer,32767).astype(scipy.int16)
        fmax=self.variable.freq_max
        RATE=int(self.variable.factor_rate*fmax)
        filename=QtGui.QFileDialog.getSaveFileName(self,'saveFile','partiel.wav',filter ='wav (*.wav *.)')
        wave_file = wave.open(filename[0], 'w')
        wave_file.setparams((2, 2, RATE, 0, 'NONE', 'not compressed'))
        for ii in range(0,scipy.size(dataW[:,0])):
            sig= wave.struct.pack('<hh', int(dataW[ii,0]),int(dataW[ii,1]))
            wave_file.writeframesraw(sig)
        wave_file.close()


###############################################################################
###############################################################################
###############################################################################
        
if __name__=='__main__':
    app = QtGui.QApplication(sys.argv)
    style = QtGui.QStyleFactory.create('Plastique')
    app.setStyle(style)
    
    ##### Display the opening screen #####
    splash=splashScreen()
    splash.show_splash()
    app.processEvents()
    QtCore.QThread.sleep(3)
    splash.close_splash()
    
    ##### Launch the interface #####
    gui = spectro()
    gui.show()
    app.exec_()# -*- coding: utf-8 -*-
    
    ##### Close the program #####
    me = os.getpid()
    kill_proc_tree(me)