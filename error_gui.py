# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'error_gui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_error(object):
    def setupUi(self, error):
        error.setObjectName("error")
        error.setEnabled(True)
        error.resize(482, 196)
        self.ok_button = QtWidgets.QPushButton(error)
        self.ok_button.setGeometry(QtCore.QRect(190, 140, 81, 31))
        self.ok_button.setCheckable(False)
        self.ok_button.setAutoDefault(False)
        self.ok_button.setObjectName("ok_button")
        self.error_text = QtWidgets.QLabel(error)
        self.error_text.setGeometry(QtCore.QRect(40, 10, 391, 121))
        font = QtGui.QFont()
        font.setFamily("Calibri")
        self.error_text.setFont(font)
        self.error_text.setScaledContents(False)
        self.error_text.setObjectName("error_text")

        self.retranslateUi(error)
        QtCore.QMetaObject.connectSlotsByName(error)

    def retranslateUi(self, error):
        _translate = QtCore.QCoreApplication.translate
        error.setWindowTitle(_translate("error", "Dialog"))
        self.ok_button.setText(_translate("error", "Ok"))
        self.error_text.setText(_translate("error", "TextLabel"))
